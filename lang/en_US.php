<?php
$lang['module_friendlyname'] = 'DoubleList';
$lang['module_description'] = 'ListItExtended Cross module / DoubleList field definition';

$lang['general'] = 'General';
$lang['about'] = 'About';
$lang['team'] = 'Team';
$lang['help_general'] = <<<EOT
<h3>General Info</h3>
    <p>DoubleList provides a field type for the <a href="http://dev.cmsmadesimple.org/projects/listit2" target="_blank">ListItExtended module</a> for better management of multi-selects data types.</p>
    <p>Use this as a drop-in replacement for the MultiSelect type.</p>
    <h3>Admin Interface View:</h3>
    <img src="https://bitbucket.org/drmikecrowe/listit2doublelist/raw/cb7b22552a8d1853f03cef03c7f6e20e9768df25/assets/doublelist.png" alt="admin" />
        <h2>Source:</h2>
        <a href="https://github.com/parvez/jQuery-Dual-Listbox">https://github.com/parvez/jQuery-Dual-Listbox</a>
        <article class="markdown-body entry-content" itemprop="mainContentOfPage"><h1>
                <a name="jquery-dual-listbox" class="anchor" href="#jquery-dual-listbox"><span class="octicon octicon-link"></span></a>jQuery-Dual-Listbox</h1>
            <p>Dual Listbox jQuery Plug-in Version 1.3</p>
            <h2>
                <a name="testing" class="anchor" href="#testing"><span class="octicon octicon-link"></span></a>Testing</h2>
            <ul>
                <li>IE8/Windows XP</li>
                <li>Firefox/Windows XP</li>
                <li>Chrome/Windows XP</li>
            </ul>
            <h2>
                <a name="license-information" class="anchor" href="#license-information"><span class="octicon octicon-link"></span></a>License Information</h2>

<pre>Developed by Justin Mead
Copyright (c) 2011 MeadMiracle
www.meadmiracle.com / meadmiracle@gmail.com
Licensed under the MIT License http://opensource.org/licenses/MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

As this Dual Listbox jQuery Plug-in was initially released with GPL v2 License which was quite restrictive, I got the following explicit permission
to release it under MIT License from the original author and copyright holder.
From: Justin Mead
Date: December 19, 2012, 4:11:28 PM PST
To: Parvez
Cc: web@meadmiracle.com
Subject: Re: Dual Listbox jQuery Plug-in
Happy to help!  You have my permission to release my code under the MIT license.  Good luck with your project!
Justin
</pre></article>
EOT;
$lang['help_about'] = <<<EOT
	<h3>About</h3>
    <p>If you find any bugs please feel free to submit a bug report <a href="http://dev.cmsmadesimple.org/bug/list/982" target="_blank">here</a> or for any good ideas consider submiting a feature request <a href="http://dev.cmsmadesimple.org/feature_request/list/982" target="_blank">here</a>. </p>
    <p>Please keep in mind that developers do have their daily jobs which means that feature requests are considered and done as time allows. If you need a feature really badly consider contacting one of the developers for a sponsored development.
    </p>
EOT;

$lang['help_param_field'] = 'Pass the ListItExtened field you want to display in front-end.';
$lang['option'] = 'Option';
$lang['fielddef_multioptions_help'] = 'Options separated by line breaks. Values can be separated from text with a = character. For example: Banana=banana';
$lang['options'] = 'Options';
$lang['fielddef_DoubleList'] = 'DoubleList';
$lang['enable_copy'] = "Copy items instead of moving";
$lang['fielddef_enable_copy_help'] = "If Copy is checked, then all the items remain in the left column remains";
?>