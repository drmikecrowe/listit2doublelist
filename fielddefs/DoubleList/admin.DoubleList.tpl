<div class="pageoverflow">
    <p class="pagetext">{$fielddef->ModLang('enable_copy')}:</p>
    <p class="pageinput">
        {$themeObject->DisplayImage('icons/system/info.gif')}<em> {$fielddef->ModLang('fielddef_enable_copy_help')}</em><br />
        <input type="hidden" name="{$actionid}custom_input[copy_buttons]" value="0" />
        <input type="checkbox" name="{$actionid}custom_input[copy_buttons]" value="1"{if $fielddef->GetOptionValue('copy_buttons') == 1} checked="checked"{/if} />
    </p>
</div>

<div class="pageoverflow">
    <p class="pagetext">{$fielddef->ModLang('options')}:</p>
    <p class="pageinput">
        {$themeObject->DisplayImage('icons/system/info.gif')}<em> {$fielddef->ModLang('fielddef_multioptions_help')}</em><br />
        <textarea name="{$actionid}custom_input[options]">{$fielddef->GetOptionValue('options')}</textarea>
    </p>
</div>

