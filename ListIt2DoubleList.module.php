<?php
#-------------------------------------------------------------------------
# Module: ListIt2DoubleList - ListIt2 Double List data type
# Version: 1.0.1, Mike Crowe
#
#-------------------------------------------------------------------------
#
# ListIt2DoubleList is a CMS Made Simple support module that adds field definitions to ListIt2 module.
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

class ListIt2DoubleList extends CMSModule
{
	public function GetName()             { return get_class($this);                                        }
	public function GetFriendlyName()     { return $this->Lang('module_friendlyname');                      }
	public function GetAdminDescription() { return $this->Lang('module_description');                       }
	public function GetDependencies()     { return array('ListIt2' => '1.4');                               }
	public function GetVersion()          { return '1.0.3';                                                 }
	public function MinimumCMSVersion()   { return '1.11';                                                  }
	public function GetAuthor()           { return 'Mike Crowe';                                            }
	public function GetAuthorEmail()      { return 'drmikecrowe@gmail.com';                                 }
	public function GetChangeLog()        { return @file_get_contents(dirname(__FILE__).'/changelog.html'); }
	public function VisibleToAdminUser()  { return false;                                                   }
	public function IsPluginModule()      { return true;                                                    }
    public function LazyLoadFrontend()    { return true;                                                    }
    public function AllowAutoInstall()    { return true;                                                    }

	public function GetHelp()
	{
		$smarty = cmsms()->GetSmarty();
		$smarty->assign('mod', $this);

		return $this->ProcessTemplate('help.tpl');
	}

	public function InitializeAdmin()
	{
		$this->CreateParameter('field', '', $this->Lang('help_param_field'));
        $this->smarty->assign("doublelist_assets",$this->GetModuleURLPath()."/assets");
	}

	public function InitializeFrontend()
	{
		$this->RegisterModulePlugin();
		$this->RestrictUnknownParams();

		$this->SetParameterType('field', CLEAN_NONE);
	}

}
?>
