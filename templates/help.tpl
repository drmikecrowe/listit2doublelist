<div id="page_tabs">
    <div id="general">
        {$mod->Lang('general')}
    </div>
    <div id="about">
        {$mod->Lang('about')}
    </div>
</div>
<div class="clearb"></div>
<div id="page_content">
    <div id="general_c">
        {$mod->Lang('help_general')}
    </div>
    <div id="about_c">
        <h3>{$mod->Lang('team')}</h3>
        <ul>
            <li>Integrated by drmikecrowe@gmail.com</li>
        </ul>
    </div>
</div>